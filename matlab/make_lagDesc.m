function [lagDesc, corrs] = make_lagDesc(nrows, ncols, corrs)
% make_lagDesc	Generate a lag description for matrix array transducers to be
%				used by spatcov_mex
%
%	[lagDesc, corrs] = make_lagDesc(nrows, ncols, ncorrs);
%
% This function generates a cell array, with each cell containing a list of
% channel pairs to be used in the spatial covariance computation by spatcov_mex.
%
%

% If ncorrs is not provided, automatically select the number of correlation
% coefficients to use
if nargin == 2
	ncorrs = max(nrows, ncols) - 1;

	% Choose the correlation coefficients that will be used to select lags
	% Exclude the 0 lag (corr is always 1) and the N lag (no such element pairs)
	corrs = linspace(1, 0, ncorrs+2);
	corrs = corrs(2:end-1);
else
	ncorrs = length(corrs);
end

% Sort corrs in descending order
corrs = sort(corrs(:), 'descend');

% Obtain the element pairs belonging to the spatial covariance matrix
[X, Y] = meshgrid(-nrows:nrows, -ncols:ncols);

% Compute the correlation coefficients for each potential element pair
R = (1 - abs(X/nrows)) .* (1 - abs(Y/ncols));

% For every single element, compute its correlation with every single other elem
lagmat = zeros(nrows*ncols, nrows*ncols);
for j = 1:ncols
	for i = 1:nrows
		% Select the window of the spatial covariance function that places
		% (i,j) at the corr = 1 position
		X_ij = (1:nrows)+nrows+1 - i;
		Y_ij = (1:ncols)+ncols+1 - j;
		R_ij = R(Y_ij,X_ij)';
		
		% Place these in lagmat
		lagmat(i + (j-1)*nrows,:) = R_ij(:);
	end
end
% lagmat is symmetric, so only use the upper triangle
lagmat(find(tril(lagmat,-1))) = nan; % Need the "find" command for this to work

% Determine the correlation cutoff points at which to bin the element pairs.
% Select the mid-point between two correlation coefficients as the cutoff:
corrCutoff = (corrs + [corrs(2:end); 0])/2;

% Split up lagmat into cutoff points and place into lagDesc
lagDesc = cell(ncorrs, 1);
[i, j] = find(lagmat >= corrCutoff(1));
lagDesc{1} = [i, j];
if corrs(1) ~= 1 % Only include lag zero if requested explicitly
	[tmpi, tmpj] = find(lagmat == 1);
	lagDesc{1} = setdiff(lagDesc{1}, [tmpi, tmpj], 'rows');
end
for m = 2:length(corrs)
	[tmpi, tmpj] = find(lagmat >= corrCutoff(m) & lagmat < corrCutoff(m-1));
	lagDesc{m} = [tmpi, tmpj];
end
