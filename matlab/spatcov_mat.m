function [ccR, ccI] = spatcov_mat(iqdata, kernelsz, lagDesc, ccmode)
% This function is a MATLAB implementation of spatcov for demonstration
%	purposes, and is only compatible with 2 pixel dimensions and a scalar
%	lagDesc.

% Parse arguments
% Get size of data
datadims = size(iqdata);
nchans = datadims(end);
npixdims = ndims(iqdata)-1;
if npixdims ~= 2
	error('This function is for demonstration purposes, and only works with 2D image data.')
end

% Get kernel size
ksz = ones(npixdims, 1);
if nargin > 1 % If user provides kernelsz
	% Make sure kernel sizes are within bounds
	assert(all(kernelsz > 0) && all(kernelsz < datadims(1:length(kernelsz))));
	for i = 1:min(npixdims, length(kernelsz))
		% Make sure kernel sizes are odd
		ksz(i) = floor(kernelsz(i)/2)*2+1;
	end
end

% Get lag information
maxlag = nchans-1; % By default, compute all lags of a 1D uniform array
if nargin > 2
	maxlag = lagDesc;
end

% Get correlation coefficient aggregation method
ccm = 0; % By default, use ensemble method
if nargin > 3
	ccm = double(ccmode > 0);
end

% Estimate spatial covariance
if nargout == 1
	ccR = estimateSpatCov2D(iqdata, ksz, maxlag, ccm);
elseif nargout == 2
	[ccR, ccI] = estimateSpatCov2D(iqdata, ksz, maxlag, ccm);
end

end

% Estimate the spatial covariance
function [ccR, ccI] = estimateSpatCov2D(iqdata, ksz, maxlag, ccm)

% Data dimensions
[nrows, ncols, nchans] = size(iqdata);
halfksz = (ksz-1)/2;

% Prepare output array
ccR = zeros(nrows, ncols, maxlag, class(iqdata));
if nargout == 2
	ccI = zeros(nrows, ncols, maxlag, class(iqdata));	
end

for col = 1:ncols
	for row = 1:nrows
		for m = 1:maxlag
			
			% Find valid kernel indices
			zidx = max(1, row-halfksz(1)) : min(nrows, row+halfksz(1));
			xidx = max(1, col-halfksz(2)) : min(ncols, col+halfksz(2));
			
			% Extract kernels from raw data
			Ia = real(iqdata(zidx,xidx,1:nchans-m));
			Qa = imag(iqdata(zidx,xidx,1:nchans-m));
			Ib = real(iqdata(zidx,xidx,m+1:nchans));
			Qb = imag(iqdata(zidx,xidx,m+1:nchans));
			
			if ccm == 0
				% If using "ensemble" ccmode, treat samples as an ensemble of
				% observations, and compute a single correlation coefficient
				Ia = Ia(:);	Qa = Qa(:);	Ib = Ib(:);	Qb = Qb(:);
			else
				% Otherwise, treat pixel dimensions as the ensemble, and compute
				% nchans-maxlag correlation coefficients, and then take their
				% average.
				Ia = reshape(Ia, length(zidx)*length(xidx), nchans-m);
				Qa = reshape(Qa, length(zidx)*length(xidx), nchans-m);
				Ib = reshape(Ib, length(zidx)*length(xidx), nchans-m);
				Qb = reshape(Qb, length(zidx)*length(xidx), nchans-m);
			end
			
			% Compute real component of correlation coefficient
			ccR(row,col,m) = mean(...
				sum(Ia.*Ib + Qa.*Qb, 1) ./ sqrt( ...
				sum(Ia.*Ia + Qa.*Qa, 1) .* ...
				sum(Ib.*Ib + Qb.*Qb, 1) ));
			
			if nargout == 2
				ccI(row,col,m) = mean( ...
					sum(Qa.*Ib - Ia.*Qb, 1) ./ sqrt( ...
					sum(Ia.*Ia + Qa.*Qa, 1) .* ...
					sum(Ib.*Ib + Qb.*Qb, 1) ));
			end
		end
	end
end

end









