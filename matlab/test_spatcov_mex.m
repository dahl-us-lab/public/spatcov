%% Choose processing parameters
ksz = [1 1];
nlags = 16;
dataFile = 'cyst_-20dB_lowRes.mat';
% dataFile = 'cyst_-20dB_hiRes.mat';

%% Estimate spatial covariance from Field II simulated data
load(dataFile)

% If spatcovMT_mex cannot be found, just execute spatcov_mex
if exist('spatcovMT_mex', 'file') ~= 3
	spatcovMT_mex = @(f,k,l,c) spatcov_mex(f,k,l,c);
end

% Compute spatial covariance and measure timing
t = zeros(6,1);
tic; cc1 = spatcov_mat  (focsig, ksz, nlags, 0); t(1) = toc; fprintf('MATLAB (ens) took %.3f seconds.\n', t(1))
tic; cc2 = spatcov_mat  (focsig, ksz, nlags, 1); t(2) = toc; fprintf('MATLAB (avg) took %.3f seconds.\n', t(2))
tic; cc3 = spatcov_mex  (focsig, ksz, nlags, 0); t(3) = toc; fprintf('MEX    (ens) took %.3f seconds.\n', t(3))
tic; cc4 = spatcov_mex  (focsig, ksz, nlags, 1); t(4) = toc; fprintf('MEX    (avg) took %.3f seconds.\n', t(4))
tic; cc5 = spatcovMT_mex(focsig, ksz, nlags, 0); t(5) = toc; fprintf('MEX MT (ens) took %.3f seconds.\n', t(5))
tic; cc6 = spatcovMT_mex(focsig, ksz, nlags, 1); t(6) = toc; fprintf('MEX MT (avg) took %.3f seconds.\n', t(6))

% Compute the mean square difference between the MATLAB and C++ implementations
fprintf('MSQ diff. between MATLAB and MEX (ens): %f\n', mean((cc3(:)-cc1(:)).^2))
fprintf('MSQ diff. between MATLAB and MEX (avg): %f\n', mean((cc4(:)-cc2(:)).^2))

%% Plot results
% Compute SLSC images from spatial covariance estimates
cimg1 = mean(cc1,3);
cimg2 = mean(cc2,3);
cimg3 = mean(cc3,3);
cimg4 = mean(cc4,3);
cimg5 = mean(cc5,3);
cimg6 = mean(cc6,3);

% Show SLSC images
figure(1),clf;
set(gcf,'Position', [100 100 800 500])
subplot(231); imagesc(img_x,img_z,cimg1/max(cimg1(:)), [0 1]); colormap gray; axis image; title('MATLAB ens')
subplot(234); imagesc(img_x,img_z,cimg2/max(cimg2(:)), [0 1]); colormap gray; axis image; title('MATLAB avg')
subplot(232); imagesc(img_x,img_z,cimg3/max(cimg3(:)), [0 1]); colormap gray; axis image; title('MEX ens')
subplot(235); imagesc(img_x,img_z,cimg4/max(cimg4(:)), [0 1]); colormap gray; axis image; title('MEX avg')
subplot(233); imagesc(img_x,img_z,cimg5/max(cimg5(:)), [0 1]); colormap gray; axis image; title('MEX MT ens')
subplot(236); imagesc(img_x,img_z,cimg6/max(cimg6(:)), [0 1]); colormap gray; axis image; title('MEX MT avg')

% Show timing plot
figure(2),clf;
set(gcf,'Position',[900 100 400 500])
relt = reshape(t, 2, 3);
relt(1,:) = relt(1,1) ./ relt(1,:);
relt(2,:) = relt(2,1) ./ relt(2,:);
relt = cat(2, relt(1,:), 0, relt(2,:));
bar(relt)
yl = ylim; yl(2) = yl(2)*1.05; ylim(yl);
hold on; plot([4 4],[0 yl(2)], 'k:'); hold off
text(4, 0.98*yl(2), 'Ensemble', 'Rotation', 90, 'VerticalAlignment','bottom', 'HorizontalAlignment','right')
text(8, 0.98*yl(2), 'Average', 'Rotation', 90, 'VerticalAlignment','bottom', 'HorizontalAlignment','right')
set(gca,'XTickLabel', {'MATLAB', 'MEX', 'MEX MT', '', 'MATLAB', 'MEX', 'MEX MT'})
set(gca,'XTickLabelRotation', 45)
title('Speedup over MATLAB implementation')
set(gca,'YGrid','On')
for i = [1:3 5:7]
	text(i, relt(i), sprintf('%dx',round(relt(i))), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center')
end
