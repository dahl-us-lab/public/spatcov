#include "spatcov_mex.h"

// MATLAB input arguments are:
// 1: data			  - Organized as rows x cols x chans, or as rows x chans
// 2: kernel size	  - Defined as [ksz_z, ksz_x] for axial and lateral size
// 3: lag description - Scalar: number of lags assuming a 1D array
//						Cell array: list of ccIndexes
// 4: aggregate type  - 0: ensemble mode
//					  - 1: averaging mode
//
// MATLAB outputs are:
// 1: ccReal - Average/ensemble correlation coefficients for each lag
// 2: ccImag - (optional) Imaginary component of average/ensemble CCs

// MATLAB gateway function
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

	// Check number of inputs
	if (nrhs < 1 || nrhs > 4)
		mexErrMsgTxt("Incorrect number of inputs.");

	// INPUT 1: DATA
	// Grab pointer to input
	mxClassID classIn = mxGetClassID(prhs[0]);
	if (classIn != mxSINGLE_CLASS && classIn != mxDOUBLE_CLASS)
		mexErrMsgTxt("Input signal must be of class single or double.");
	// Assert that data is complex.
	if (!mxIsComplex(prhs[0]))
		mexErrMsgTxt("Input data must be complex.");
	// Determine data dimensions
	mwSize ndims = mxGetNumberOfDimensions(prhs[0]);
	const mwSize *dims = mxGetDimensions(prhs[0]);
	// Get the number of pixel dimensions. (Last dimension is always channel dimension.)
	mwSize npdims = ndims - 1;
	mwSize nchans = dims[ndims-1];

	// INPUT 2: KERNEL SIZE
	// Default value for kernel size is 1 in all pixel dimensions
	size_t kernsz[NPDIMS_MAX];
	for (size_t i = 0; i < npdims; i++) { kernsz[i] = 1; }
	// If user provides kernel sizes, use them
	if (nrhs >= 2) {
		double *userKernsz = mxGetPr(prhs[1]);
		for (size_t i = 0; i < npdims && i < mxGetNumberOfElements(prhs[1]); i++)
			kernsz[i] = (size_t) userKernsz[i];
	}

	// INPUT 3: LAG DEFINITION
	// Default is to assume a 1D array and to use all lags
	size_t nlags = nchans-1;
	size_t *lagSizes = NULL;
	size_t *ccIndexes = NULL;
	// Lags to compute
	if (nrhs >= 3) {
		// If the user provides a scalar threshold value:
		if (!mxIsCell(prhs[2])) {
			if (mxGetNumberOfElements(prhs[2]) == 1)
				nlags = (size_t) mxGetScalar(prhs[2]);
			else
				mexErrMsgTxt("Expected scalar or cell array for lag definition.\n");
		}
		// Otherwise, if the user provides a cell array containing custom lag definition:
		else {
			nlags = mxGetNumberOfElements(prhs[2]);
			const mxArray *lagCellArray = prhs[2];
			// Compute how many cc's are computed for each lag
			lagSizes = (size_t *)mxMalloc(sizeof(size_t)*nlags);
			size_t nTotalCCs = 0;
			for (size_t lag = 0; lag < nlags; lag++) {
				mxArray *lagCell = mxGetCell(lagCellArray, lag);
				lagSizes[lag] = mxGetM(lagCell);
				nTotalCCs += lagSizes[lag];
			}
			// Store these values in the ccIndexes array
			ccIndexes = (size_t *)mxMalloc(sizeof(size_t)*nTotalCCs);
			size_t ccIdx = 0;
			for (size_t lag = 0; lag < nlags; lag++) {
				mxArray *lagCell = mxGetCell(lagCellArray,lag);
				double *cc = mxGetPr(lagCell);
				for (size_t i = 0; i < lagSizes[lag]; i++) {
					// Get element pair (a,b) from the i-th row of lagCell
					size_t a = (size_t)cc[i + 0*lagSizes[lag]] - 1; // -1 for zero-index
					size_t b = (size_t)cc[i + 1*lagSizes[lag]] - 1; // -1 for zero-index
					// Convert (a,b) into the covariance matrix index
					ccIndexes[ccIdx] = a + nchans*b;
					ccIdx++;
				}
			}
		}
	}

	// INPUT 4: Averaging vs. ensemble mode
	// Use ensemble mode by default
	int ccmode = 0; // 0 for ensemble CC, 1 for averaging CCs
	if (nrhs >= 4) {
		if (!mxIsEmpty(prhs[3])) {
			ccmode = (int)mxGetScalar(prhs[3]);
		}
	}

	// OUTPUT 1: Real correlation coefficients
	mwSize outdims[NPDIMS_MAX];
	for (int i = 0; i < ndims-1; i++)
		outdims[i] = dims[i];
	outdims[ndims-1] = nlags;
	plhs[0] = mxCreateNumericArray(ndims, outdims, classIn, mxREAL);

	// OUTPUT 2: Imaginary correlation coefficients
	bool complexOutput = false;
	if (nlhs > 1) {
		complexOutput = true;
		plhs[1] = mxCreateNumericArray(ndims, outdims, classIn, mxREAL);
	}

	// Assign pointers to data
	void *I, *Q, *ccR, *ccI;
	I = mxGetData(prhs[0]);
	Q = mxGetImagData(prhs[0]);
	ccR = mxGetData(plhs[0]);
	ccI = complexOutput ? mxGetData(plhs[1]) : NULL;
	
	// PREPROCESSING
	// Make sure that the data is not too large or too small to avoid
	// errors with precision when multiplying values together.
	// For instance, if the data is from Field II simulations, all signals
	// will be ~10^-20, subjecting the result to NaN poisoning.
	if (classIn == mxDOUBLE_CLASS) {
		double rmsValue = checkDataRMS((double *)I,
									   (double *)Q,
									   mxGetNumberOfElements(prhs[0]));
		if (rmsValue < 1e-10 || rmsValue > 1e10) {
			mexErrMsgTxt("The data values are too large or small. Normalize to avoid losing precision.\n");
//			mexPrintf("Normalizing data...\n");
//			normalizeData((double *) I,
//						  (double *) Q,
//						  mxGetNumberOfElements(prhs[0]),
//						  rmsValue);
		}
	}
	else if (classIn == mxSINGLE_CLASS) {
		double rmsValue = checkDataRMS((float *)I,
									   (float *)Q,
									   mxGetNumberOfElements(prhs[0]));
		if (rmsValue < 1e-10 || rmsValue > 1e10) {
			mexErrMsgTxt("The data values are too large or small. Normalize to avoid losing precision.\n");
//			mexPrintf("Normalizing data...\n");
//			normalizeData((float *) I,
//						  (float *) Q,
//						  mxGetNumberOfElements(prhs[0]),
//						  rmsValue);
		}
	}
	else {
		mexErrMsgTxt("Unsupported data type.\n");
	}

	// ESTIMATE SPATIAL COHERENCE
	if (classIn == mxDOUBLE_CLASS) {
		computeCoherence(ccmode,
						 CCTYPE,
						 ndims,
						 dims,
						 (double *)I,
						 (double *)Q,
						 kernsz,
						 (double *)ccR,
						 (double *)ccI,
						 nlags,
						 lagSizes,
						 ccIndexes);
	}
	else if (classIn == mxSINGLE_CLASS) {
		computeCoherence(ccmode,
						 CCTYPE,
						 ndims,
						 dims,
						 (float *)I,
						 (float *)Q,
						 kernsz,
						 (float *)ccR,
						 (float *)ccI,
						 nlags,
						 lagSizes,
						 ccIndexes);
	}
	else {
		mexErrMsgTxt("Unsupported data type.\n");
	}

	// Free memory if allocated
	mxFree(lagSizes);
	mxFree(ccIndexes);
}
